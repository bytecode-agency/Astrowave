<?php

//Text verwijderen
add_filter( 'gettext', 'replace_preview_resume_text' );
 
function replace_preview_resume_text( $text ){
 
    if( $text === 'Dream-Theme — truly premium WordPress themes' ) {
        $text = '';
    }
 
    return $text;
}

function romanic_number($integer, $upcase = true) 
{ 
    $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1); 
    $return = ''; 
    while($integer > 0) 
    { 
        foreach($table as $rom=>$arb) 
        { 
            if($integer >= $arb) 
            { 
                $integer -= $arb; 
                $return .= $rom; 
                break; 
            } 
        } 
    } 

    return $return; 
} 

// Shortcodes
add_shortcode('carouselagent', 'carouselagent');
add_shortcode('bestel', 'bestel');
add_shortcode('credits', 'credits');

function carouselagent(){ 
global $wpdb;
//$agent_rs = $wpdb->get_results( "SELECT * FROM agents d inner join agents_status cd on d.status = cd.status order by cd.ID",ARRAY_A );
$agent_rs = $wpdb->get_results( "SELECT * FROM agents d inner join agents_status cd on d.status = cd.status order by cd.ID, boxnummer",ARRAY_A );
	if(!empty($agent_rs)){
		foreach($agent_rs as $row_agents){?>		
		<div class="col-1-4">
				<div class="mediumbox">
					<div class="nummer tekst">Boxnummer: <strong><?php echo $row_agents['boxnummer']; ?></strong></div>
						<div class="boxinner">
							<img src="/script/pf/<?php echo $row_agents['Afbeelding'] ?>.png" class="afbeeldingmedium">
							<div class="profiel tekst"><?php echo $row_agents['Profiel']; ?></div>
							<div class="status-container">	
								<img alt="<?php echo $row_agents['status']; ?>" src="/script/image/<?php echo ucfirst($row_agents['status']) ?>.gif" class="status-img">
								<div class="status tekst"><?php echo ucfirst($row_agents['statusweb']) ?></div>
								<div class="leesmeer tekst"><a href="/medium/<?php echo $row_agents['Afbeelding']; ?>"><b><u>Lees meer</u></b></a></div>
							</div>
						</div>
					<div class="naam tekst"><?php echo $row_agents['naam']; ?></div>
				</div>
			</div><?php	
		}
	}
}

function bestel(){ include 'script/bestel.php'; }
function credits(){ include 'script/credits1.php'; } //Was eerst credits.php

// Cronjob
// add_action('carouselagent', 'carouselagent');

// Vertalen
add_filter( 'gettext', 'replace_text_1' );function replace_text_1( $text ){    if( $text === 'by' ) {        $text = 'door';    }    return $text;}
add_filter( 'gettext', 'replace_text_2' );function replace_text_2( $text ){    if( $text === 'Submit your review' ) {        $text = 'In ons gastenboek kan je berichten plaatsen . Deze zijn door iedereen te bekijken.';    }    return $text;}
add_filter( 'gettext', 'replace_text_3' );function replace_text_3( $text ){    if( $text === 'Click here to hide form' ) {        $text = 'Nieuw bericht plaatsen in het AOM-gastenboek';    }    return $text;}
add_filter( 'gettext', 'replace_text_4' );function replace_text_4( $text ){    if( $text === 'Rating' ) {        $text = 'Beoordeling';    }    return $text;}
add_filter( 'gettext', 'replace_text_5' );function replace_text_5( $text ){    if( $text === '::' ) {        $text = ':';    }    return $text;}
add_filter( 'gettext', 'replace_text_6' );function replace_text_6( $text ){    if( $text === 'Required Field' ) {        $text = 'Verplicht veld';    }    return $text;}
add_filter( 'gettext', 'replace_text_7' );function replace_text_7( $text ){    if( $text === 'Check this box to confirm you are human.' ) {        $text = 'Plaats mijn bericht in het gastenboek';    }    return $text;}


?>