<?php

//Text verwijderen
add_filter( 'gettext', 'replace_preview_resume_text' );
 
function replace_preview_resume_text( $text ){
 
    if( $text === 'Dream-Theme — truly premium WordPress themes' ) {
        $text = '';
    }
 
    return $text;
}

// Shortcodes
add_shortcode('carouselagent', 'carouselagent');
add_shortcode('bestel', 'bestel');
add_shortcode('credits', 'credits');

// Functions 
//function carouselagent(){ include 'script/carouselagent_update.php'; } Old Code comment
//Fixed by Bill Daghay : Email Address : bill.daghay@gmail.com 
function carouselagent(){ 
global $wpdb;
$agent_rs = $wpdb->get_results( "SELECT * FROM agents d inner join agents_status cd on d.status = cd.status order by cd.ID",ARRAY_A );
	if(!empty($agent_rs)){
		foreach($agent_rs as $row_agents){?>
		<div class="col-1-4">
				<div class="mediumbox">
					<img src="/script/pf/<?php echo $row_agents['Afbeelding'] ?>.png" class="afbeeldingmedium">
					<div class="naampaars"><?php echo $row_agents['naam']; ?></div>
					<div class="profiel"><?php echo $row_agents['Profiel']; ?></div>
					<br>
					<img alt="<?php echo $row_agents['naam']; ?>" src="/script/image/<?php echo ucfirst($row_agents['status']) ?>.gif" style="float: left; margin-left: 10px; padding-right: 5px; padding-top: 5px;">
					<div class="status"><?php echo ucfirst($row_agents['status']) ?></div>
					<div class="leesmeer"><a href="/medium/<?php echo $row_agents['Afbeelding']; ?>"><b><u>Lees meer</b></u></a></div>
					<br>
					<div class="nummer">Boxnummer: <strong><?php echo $row_agents['boxnummer']; ?></strong></div>
				</div>
			</div><?php	
		}
	}
}

function bestel(){ include 'script/bestel.php'; }
function credits(){ include 'script/credits.php'; }

// Cronjob
add_action('carouselagent', 'carouselagent');

// Vertalenadd_filter( 'gettext', 'replace_text_1' );function replace_text_1( $text ){    if( $text === 'by' ) {        $text = 'door';    }    return $text;}add_filter( 'gettext', 'replace_text_2' );function replace_text_2( $text ){    if( $text === 'Submit your review' ) {        $text = 'In ons gastenboek kan je berichten plaatsen . Deze zijn door iedereen te bekijken.';    }    return $text;}add_filter( 'gettext', 'replace_text_3' );function replace_text_3( $text ){    if( $text === 'Click here to hide form' ) {        $text = 'Nieuw bericht plaatsen in het AOM-gastenboek';    }    return $text;}add_filter( 'gettext', 'replace_text_4' );function replace_text_4( $text ){    if( $text === 'Rating' ) {        $text = 'Beoordeling';    }    return $text;}add_filter( 'gettext', 'replace_text_5' );function replace_text_5( $text ){    if( $text === '::' ) {        $text = ':';    }    return $text;}add_filter( 'gettext', 'replace_text_6' );function replace_text_6( $text ){    if( $text === 'Required Field' ) {        $text = 'Verplicht veld';    }    return $text;}add_filter( 'gettext', 'replace_text_7' );function replace_text_7( $text ){    if( $text === 'Check this box to confirm you are human.' ) {        $text = 'Plaats mijn bericht in het gastenboek';    }    return $text;}


?>