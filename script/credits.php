<!DOCTYPE html>
<html>

<head>


<script>
function validate() {
	if(document.payment_select.mail.value == '' || document.payment_select.mail.value == null) {
		alert('Er is geen e-mailadres ingegeven!');
		document.payment_select.mail.focus();
		return false;
	}
    	
	/* check email content */
	if ((/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.payment_select.mail.value)) == false)	{
		alert('Er is geen geldig e-mailadres ingegeven!');
		document.payment_select.mail.focus();
		return false;
	}
}
</script>
<style>
	body {padding:0px;margin:0px;font-size:13px;padding:15px;}
	* {font-family:Arial, Helvetica, sans-serif;}

</style>

</head>

<body>

<div id="content">

	<h1>Bel goedkoper, vanuit alle landen</h1>

	</p>

	<form class="contact" name="payment_select" action="/script/bestel.php" method="post" onsubmit="return validate();" style="background: transparent;">
		
		<h2>Uw e-mail adres</h2>
		<p>Vul hieronder uw e-mail adres in om het betaalproces te starten.<br />
			Na succesvolle betaling krijgt u per e-mail uw code toegestuurd.</p>		
		<p><span class="lable">E-mail*: </span> <input class="text" type="text" name="mail" size="50" value="" /></p>
		<br>
		<h2>Kies uw betaalwijze*:</h2>

		<table width="270" border="0" cellspacing="0">			
  			<tr>
    			<td>
					<p style="margin-top: 20px; float:left"  >
						<span class="lable" style="float:left; width: 20px; margin: 0"><input type = "radio" class="payment_select" name="paymentType" value="Bancontact" id="Bancontact" style="float:left; margin: 0; width: 20px" /></span>
						 <label for="Bancontact">
						<span style="float:left; margin-top: -25px; height: 67;" >
							<img src="/script/img/logos/mistercash.png" alt="Bancontact" width="70" height="70" />
						</span>
						<span style="float:left; height: 35; margin-top: 3px; margin-left: 13px;"><strong>Bancontact / Mister Cash</strong></span>
						</label>
					</p>
    			</td>
  			</tr>	
  			<tr>
   				<td>
					<p style="margin-top: 20px; float:left">
						<span class="lable" style="float:left; width: 20px; margin: 0;"><input type = "radio" class="payment_select" name="paymentType" value="iDeal" id="iDeal" checked style="float:left; margin: 0; width: 20px" /></span>
						<label for="iDeal">
						<span style="float:left; margin-top: -17px; height: 67;" >
							<img src="/script/img/logos/ideal.png" alt="iDeal" width="60" height="60">
						</span>
						 <label for="iDeal">
						<span style="float:left; height: 67; margin-top: 3px; margin-left: 24px;"><strong>iDeal</strong></span>
						<span style="float:left; margin-top: -1px; font-size: 10px; padding-left: 60px; color: #afafaf">ABN Amro / Rabobank / ING Bank / SNS Reaal / Fortis / ASN Bank / SNS Regiobank / Triodos Bank / Van Lanschot / Knab</span>
						</label>
					</p>
	    		</td>
  			</tr>
  			
  			
		</table>

		<br />
		<div>
			<input class="submit" type="submit" name="submit" value="Betaling starten" />
		</div>
	</form>
</div>

</body>
</html>