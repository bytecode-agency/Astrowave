<!DOCTYPE html>
<html>
<head>
<script>
    function validatmail(mail)
    {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(mail);
    }

    function hideErrors()
    {
        document.getElementById('error_0').style.display='none';
        document.getElementById('error_1').style.display='none';
        document.getElementById('error_2').style.display='none';
    }

    function showError(n)
    {
        if (n==0) 		{hideErrors();document.getElementById('error_0').style.display='block';}
        else if (n==1) 	{hideErrors();document.getElementById('error_1').style.display='block';}
        else if (n==2) 	{hideErrors();document.getElementById('error_2').style.display='block';}
    }

    function startPay()
    {
        hideErrors();
        var e=document.getElementById('mail').value;
        var l=document.getElementById('paymentType');
        var paymentType=l.options[l.options.selectedIndex].value;
        if (paymentType!="Bancontact" && paymentType!="Giropay" && paymentType!="PayPal" && paymentType!="iDeal" && paymentType!="Creditcard" && paymentType!="Overboeking")
        {showError(2);return;}

        if (e=="") {showError(0);}
        else if(validatmail(e)==false) {showError(1);}
        else
        {
            document.getElementById('inputForm').style.display='none';
            if (paymentType=="PayPal") {document.forms["inputForm"].submit();}
            else if(paymentType=="Giropay") {document.forms["inputForm"].submit();}
            else if(paymentType=="Creditcard") {document.forms["inputForm"].submit();}
            else if(paymentType=="Bancontact") {document.forms["inputForm"].submit();}
            else if(paymentType=="Overboeking") {document.forms["inputForm"].submit();}
            else if(paymentType=="iDeal") {document.forms["inputForm"].submit();}
            else {showError(0)}
        }
    }
</script>

<style>
	body {padding:0px;margin:0px;font-size:13px;padding:15px;}
	* {font-family:Arial, Helvetica, sans-serif;}
	#inputForm 	{display:block;}
	#myIframe 	{display:block;}
	.error {color:#FFF;background:#DD3333;padding:3px;display:none;}
	.textbox {border:1px solid #999;width:240px;}
	.button {border:1px solid #999;}
</style>

</head>

<body>

	<div id="content">
		<h1 class="pagetitle">Credits</h1>
			<form id="inputForm" action="bestel.php" method="post">
			<p><b>BetalingspaymentType</b><br/>Kies uw betaalwijze.</label></p>
			<div id="error_2" class="error">Kies a.u.b. uw betaalwijze.</div>
				<table>
					<tr>
						<td style="width:100px;"><label>Betaalwijze:</td>
					<td>
					<select id="paymentType" name="paymentType" class="textbox">
						<option value="">Kies uw betaalwijze...</option>
						<option value="Bancontact">Bancontact/Mister cash</option>
<!--				<option value="Creditcard">Creditcard</option>					-->		
<!--				<option value="Giropay">Giropay</option>								-->
						<option value="iDeal">IDeal</option>
<!--				<option value="Overboeking">Overboeking</option>				-->
<!--				<option value="PayPal">PayPal</option> 									-->		
					</select></td></tr>
				</table>
			<p><b>Uw e-mailadres</b><br/>Vul hieronder uw e-mailadres in om het betaalproces te starten.<br>Na succesvolle betaling krijgt u per e-mail uw code toegestuurd.</p>
			<div id="error_0" class="error">Vul a.u.b. uw e-mailadres in.</div>
			<div id="error_1" class="error">Vul a.u.b. een geldig e-mailadres in.</div>
			<table>
				<tr><td style="width:100px;">Uw e-mail adres:</td><td><input type="text" class="textbox" id="mail" name="mail"/></td></tr>
				<tr><td>&nbsp;</td><td><input class="button" onclick="startPay();" value="Betaling starten"/></td></tr>
			</table>
		</form>
	</div>
	
</body>
</html>