<?php


$mail = $_POST['mail'];
$paymentType = $_POST['paymentType'];
$paymentAmount = $_POST['paymentAmount'];

if ($paymentType == "iDeal")
	{	
		if ($paymentAmount == "15.00")
			{
				$websiteId=3;
				$websiteLocationId=1;
				$Extra2="0.89";	
			}
		elseif ($paymentAmount == "25.00")
			{
				$websiteId=4;
				$websiteLocationId=1;
				$Extra2="0.89";	
			}
		elseif ($paymentAmount == "30.00")
			{
				$websiteId=5;
				$websiteLocationId=1;
				$Extra2="0.88";	
			}
		elseif ($paymentAmount == "40.00")
			{
				$websiteId=6;
				$websiteLocationId=1;
				$Extra2="0.87";	
			}
		elseif ($paymentAmount == "60.00")
			{
				$websiteId=7;
				$websiteLocationId=1;
				$Extra2="0.81";	
			}
		elseif ($paymentAmount == "80.00")
			{
				$websiteId=8;
				$websiteLocationId=1;
				$Extra2="0.80";	
			}			
		else			// Error afhandeling oude NL
			{		
				$websiteId=1;
				$websiteLocationId=1;
				$Extra2="1.00";	
			}
	}	
elseif ($paymentType == "Bancontact")
	{	
		if ($paymentAmount == "15.00")
			{
				$websiteId=9;
				$websiteLocationId=1;
				$Extra2="1.25";	
			}
		elseif ($paymentAmount == "25.00")
			{
				$websiteId=10;
				$websiteLocationId=1;
				$Extra2="1.25";	
			}
		elseif ($paymentAmount == "30.00")
			{
				$websiteId=11;
				$websiteLocationId=1;
				$Extra2="1.15";	
			}
		elseif ($paymentAmount == "40.00")
			{
				$websiteId=12;
				$websiteLocationId=1;
				$Extra2="1.14";	
			}
		elseif ($paymentAmount == "60.00")
			{
				$websiteId=13;
				$websiteLocationId=1;
				$Extra2="1.13";	
			}
		elseif ($paymentAmount == "80.00")
			{
				$websiteId=14;
				$websiteLocationId=1;
				$Extra2="1.00";	
			}			
		else			// Error afhandeling oude BE
			{		
				$websiteId=2;
				$websiteLocationId=1;
				$Extra2="1.50";	
			}
	}		

	
// Setup array with parameters
$arrParams = array();
$arrParams['programId'] = REPLACE_ME;
$arrParams['websiteId'] = $websiteId;
$arrParams['websiteLocationId'] = $websiteLocationId;
$arrParams['ipAddress'] =  $_SERVER['REMOTE_ADDR']; //Het veld IP-adres moet van de bezoeker zijn  .
//$arrParams['ipAddress'] =  "10.20.30.40"; //Het veld IP-adres moet van de bezoeker zijn  .
$arrParams['extra1'] = $mail;
//$arrParams['extra2'] = "1.00";
$arrParams['extra2'] = $Extra2;
$arrParams['extra3'] = "261";

// Setup API Url
$strUrl = "https://rest-api.pay.nl/v1/Session/create/array_serialize/";
// Prepare complete API URL
$strUrl = prepareHttpGet($strUrl, $arrParams);
// Do request
$arrResult = unserialize(@file_get_contents($strUrl));
if(is_array($arrResult) && intval($arrResult['result']) > 0)
{
  header("Location: https://www.pay.nl/betalen/?payment_session_id=".$arrResult['result']);
}
else
{
  echo "Error in generating Session";
}
function prepareHttpGet($strUrl, array $arrParams)
{
  $first = 1;
  // Prepare query string
  foreach ($arrParams as $key => $value)
  {
	if ($first != 1)
	{
	  $strUrl = $strUrl . "&";
	}
	else
	{
	  $strUrl = $strUrl . "?";
	  $first = 0;
	}
	if(is_array($value))
	{
	  foreach ($value as $k => $v)
	  {
		$strUrl = $strUrl . $key."[".$k ."]=" . $v;
	  }
	  continue;
	}
	// Add item to string
	$strUrl = $strUrl . $key ."=" . $value;
  }
  return $strUrl;
}